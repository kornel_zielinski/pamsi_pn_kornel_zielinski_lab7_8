CXXFLAGS=-g -Iinc -pedantic -std=c++11
tablica: obj obj/grafy.o obj/timer.o obj/grafy_tablica.o obj/main_tablica.o
	g++ ${CXXFLAGS} -o tablica obj/main_tablica.o obj/grafy_tablica.o obj/timer.o obj/grafy.o
grafy: obj obj/main.o obj/grafy.o obj/timer.o
	g++ ${CXXFLAGS} -o grafy obj/main.o obj/grafy.o obj/timer.o
lista: obj obj/grafy.o obj/timer.o obj/grafy_lista.o obj/main_lista.o
	g++ ${CXXFLAGS} -o lista obj/main_lista.o obj/grafy_lista.o obj/timer.o obj/grafy.o
obj:
	mkdir obj
obj/main.o: src/main.cpp
	g++ -c ${CXXFLAGS} -o obj/main.o src/main.cpp
obj/main_tablica.o: src/main_tablica.cpp 
	g++ -c ${CXXFLAGS} -o obj/main_tablica.o src/main_tablica.cpp
obj/main_lista.o: src/main_lista.cpp
	g++ -c ${CXXFLAGS} -o obj/main_lista.o src/main_lista.cpp
obj/grafy.o: src/grafy.cpp inc/grafy.hh
	g++ -c ${CXXFLAGS} -o obj/grafy.o src/grafy.cpp
obj/timer.o: src/timer.cpp inc/timer.h
	g++ -c ${CXXFLAGS} -o obj/timer.o src/timer.cpp
obj/grafy_tablica.o: src/grafy_tablica.cpp inc/grafy_tablica.hh
	g++ -c ${CXXFLAGS} -o obj/grafy_tablica.o src/grafy_tablica.cpp
obj/grafy_lista.o: src/grafy_lista.cpp inc/grafy_lista.hh
	g++ -c ${CXXFLAGS} -o obj/grafy_lista.o src/grafy_lista.cpp
