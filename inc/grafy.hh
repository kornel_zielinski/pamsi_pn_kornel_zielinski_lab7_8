#ifndef GRAFY_HH
#define GRAFY_HH

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cmath>
#include <sys/time.h>
#include <vector>

namespace base
{

  class Vert
  {
  public:
    std::string _name;
    Vert(std::string name) {_name=name;}
    std::string name() {return _name;}
  };

  class Edge
  {
  public:
    int _value;
    Vert* _vert[2];
    Edge(int value, Vert* vert1, Vert* vert2);
    int value() {return _value;}
    Vert** end_vertices();
  };

  class Graph
  {
  public:
    std::vector<Vert*> _vert;
    std::vector<Edge*> _edge;
    void insert_vertex(std::string name);
    void insert_edge(int, std::string, std::string);
    void insert_edge(int, Vert*, Vert*);
    std::vector<Vert> end_vertices(std::string name);
    ~Graph() {_vert.clear(); _edge.clear();}   
    void print();
  };

}

#endif
