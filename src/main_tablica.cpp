#include "grafy_tablica.hh"

int main()
{
  tab::Graph ayy(10);
  ayy.insert_vertex("AYY");
  ayy.insert_vertex("LMAO");
  ayy.insert_edge(10, "AYY", "LMAO");
  ayy.insert_edge(20, "AYY", "FUG" );
  ayy.print();
  if(ayy.are_adjacent("AYY", "LMAO"))
    std::cout << "AYY LMAO som\n";
  if(ayy.are_adjacent("FUG", "LMAO"))
        std::cout << "FUG LMAO som\n";
  return 0;   
}
